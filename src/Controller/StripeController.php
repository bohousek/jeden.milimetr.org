<?php

namespace App\Controller;

use Stripe\Charge;
use Stripe\Stripe;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class StripeController extends AbstractController
{
	/**
	 * @Route("/stripe", name="stripe") 
	 */
    	public function index(): Response
    {
        return $this->render(
            'intro.html.twig', [
            'controller_name' => 'StripeController',
        ]);
    }

    /**
     * @Route("/checkout", name="order_checkout")
     */
    public function checkoutAction(Request $request)
    {
//        $products = $this->get('shopping_cart')->getProducts();
        if ($request->isMethod('POST')) {
            $token = $request->request->get('stripeToken');
            // my test token
            $token = "tok_mastercard";
            Stripe::setApiKey(
                "sk_test_51J5z56JHoP3GWD9wOcy5ez43jNUc0DWoAlmfrnwioMZDxKISkMf34ygTEdtEsallM0WOKhsyEXYfYXw9q6YYxNkB00qXLCwL5U");
            Charge::create(
                array(
                    "amount" => 1 * 100,
                    "currency" => "usd",
                    "source" => $token,
                    "description" => "First test charge!"
                ));
//            $this->get('shopping_cart')->emptyCart();
            $this->addFlash('success', 'Order Complete! Yay!');
            return $this->redirectToRoute('success');
        }
        $products = [
            [
                "name" => "Pismo 1",
                "price" => 30,
            ]
        ];
        $cart = [
            "total" => 30
        ];
        return $this->render(
            'stripe/index.html.twig', array(
            'products' => $products,
            'cart' => $cart
        ));
    }

    /**
     * @Route("/success", name="success")
     * @return Response
     */
    public function success(): Response
    {
        return $this->render(
            'stripe/success.html.twig');
    }
}

<?php

namespace App\Controller;


use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Mailer\Exception\TransportExceptionInterface;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Email;
use Symfony\Component\Routing\Annotation\Route;

class SengridController extends AbstractController
{
    /**
     * @Route("/sendgrid", name="sendgrid")
     */
    public function sendEmail(MailerInterface $mailer): Response
    {
        //create and send email with twig template
        $email = (new Email())
            ->from("tisovak@gmail.com")
            ->to("jakub@stdio.cz")
            ->subject("Welcome!")
            ->text("I am sending my first email via sengrid gateway!")
            ->html("<p>Hey! This is my first email via sengrid</p>");

        try {
            $mailer->send($email);
        } catch (TransportExceptionInterface $e) {
        }
        return $this->render('sendgrid/success.html.twig');
    }
}